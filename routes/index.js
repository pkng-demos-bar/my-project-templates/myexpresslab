var express = require('express');
var router = express.Router();


// a variable to save a session
var session;

/* GET home page. */
router.get('/', function(req, res, next) {

  session = req.session;
  if(session.userid){
    res.render('users', { username: session.userid });
  }else{
    res.render('index', { title: 'Express' });
  }

});

module.exports = router;
