var express = require('express');
var router = express.Router();
var login = require('../controller/authenticate/login');
var session;

/* GET users listing. */
router.get('/', function (req, res, next) {

    session = req.session;
    if(session.userid){
        res.render('users', { username: session.userid });
    }else {
        res.render('index', { title: 'Express' });
    }

    res.send('respond with a resource');
});

/* GET users listing. */
router.post('/login', function (req, res, next) {

    const username = req.body.username;
    let loginResult = login(username, req.body.password);

    if (loginResult) {
        session = req.session;
        session.userid = req.body.username;
        console.log(req.session);
        res.render('users', {username: username});
    }
    else {
        res.render('index', {error: true, message: "Login failed!"});
    }
});

router.get('/logout', (req,res) =>{
    req.session.destroy();
    res.redirect('/');
});


module.exports = router;
